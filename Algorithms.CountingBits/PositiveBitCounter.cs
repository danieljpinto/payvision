﻿// -----------------------------------------------------------------------
// <copyright file="BitCounter.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Payvision.CodeChallenge.Algorithms.CountingBits
{
    using System;
    using System.Collections.Generic;

    public class PositiveBitCounter
    {
        public IEnumerable<int> Count(int input)
        {
            if (input < 0)
            {
                throw new ArgumentException();
            }

            IEnumerable<int> output = Calculate(input);
            
            return output;
        }

        private static IEnumerable<int> Calculate(int input)
        {
            int index = 0;
            List<int> indexes = new List<int>();

            while (input != 0)
            {
                if ((input & 1) != 0)
                {
                    indexes.Add(index);
                }

                input = input >> 1;

                index++;
            }

            indexes.Insert(0, indexes.Count);

            return indexes.ToArray();
        }
    }
}