﻿// -----------------------------------------------------------------------
// <copyright file="FraudRadar.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    using System.Collections.Generic;

    public partial class FraudRadar
    {
        private readonly string _filePath;

        public FraudRadar(string filePath)
        {
            _filePath = filePath;
        }

        public IEnumerable<FraudResult> Check()
        {
            if (string.IsNullOrEmpty(_filePath))
            {
                throw new ArgumentException("The FilePath cannot be null or empty.");
            }

            var frp = new FraudRadarProcessor();

            List<Order> orders = frp.GetNormalizedOrdersFromFile(_filePath);

            List<FraudResult> fraudResults = frp.CheckFraud(orders);

            return fraudResults;
        }
    }
}