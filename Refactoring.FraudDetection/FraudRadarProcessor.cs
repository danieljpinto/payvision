﻿// -----------------------------------------------------------------------
// <copyright file="FraudRadar.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------


namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class FraudRadarProcessor
    {
        public List<FraudRadar.Order> GetNormalizedOrdersFromFile(string filePath)
        {
            List<FraudRadar.Order> orders = ReadFileContent(filePath);

            return orders.Normalize();
        }

        public List<FraudRadar.FraudResult> CheckFraud(List<FraudRadar.Order> orders)
        {
            var fraudFilterProcessor = new FraudFilterProcessor();

            List<FraudRadar.Order> fraudList = fraudFilterProcessor.GetFraudResults(orders);

            return fraudList.ToFraudResultList();
        }

        private static List<FraudRadar.Order> ReadFileContent(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException(filePath);
            }

            const int valuesExpectedInOneLine = 8;

            var orders = new List<FraudRadar.Order>();

            var lines = File.ReadAllLines(filePath);

            for (var index = 0; index < lines.Length; index++)
            {
                string line = lines[index];

                string[] items = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    if (items.Length != valuesExpectedInOneLine)
                    {
                        LogInvalidLine(index);
                        continue;
                    }

                    var order = new FraudRadar.Order
                    {
                        OrderId = int.Parse(items[0]),
                        DealId = int.Parse(items[1]),
                        Email = items[2].ToLower(),
                        Street = items[3].ToLower(),
                        City = items[4].ToLower(),
                        State = items[5].ToLower(),
                        ZipCode = items[6],
                        CreditCard = items[7]
                    };

                    orders.Add(order);
                }
                finally
                {
                    // ... Possible implementation would be to register in someplace the 
                    // lines that went wrong with parsing to future validation
                    LogInvalidLine(index);
                }
            }

            return orders;
        }

        private static void LogInvalidLine(int index)
        {
            Console.WriteLine($"Line number {index + 1} is invalid.");
        }
    }
}