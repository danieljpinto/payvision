﻿// -----------------------------------------------------------------------
// <copyright file="FraudRadar.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    using System;
    public class OrderFieldNormalization
    {
        public void EmailFieldNormalized(FraudRadar.Order order)
        {
            //Normalize email
            var aux = order.Email.Split(new[] { '@' }, StringSplitOptions.RemoveEmptyEntries);

            var atIndex = aux[0].IndexOf("+", StringComparison.Ordinal);

            aux[0] = atIndex < 0 ? aux[0].Replace(".", "") : aux[0].Replace(".", "").Remove(atIndex);

            order.Email = string.Join("@", aux[0], aux[1]);
        }

        public void StreetFieldNormalized(FraudRadar.Order order)
        {
            // Daniel: This could came from other place like a db or a config file

            var localDic = new Dictionary<string, string>
            {
                {"st.", "street"},
                {"rd.", "road"}
            };

            foreach (KeyValuePair<string, string> valuePair in localDic)
            {
                order.Street = order.Street.Replace(valuePair.Key, valuePair.Value);
            }
        }

        public void StateFieldNormalized(FraudRadar.Order order)
        {
            // Daniel: This could came from other place like a db or a config file

            var localDic = new Dictionary<string, string>
            {
                {"il", "illinois"},
                {"ca", "california"},
                {"ny", "new york" }
            };

            foreach (KeyValuePair<string, string> valuePair in localDic)
            {
                order.State = order.State.Replace(valuePair.Key, valuePair.Value);
            }
        }
    }
}