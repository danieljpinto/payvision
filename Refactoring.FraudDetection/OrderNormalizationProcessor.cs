﻿// -----------------------------------------------------------------------
// <copyright file="FraudRadar.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    using System;
    using System.Collections.Generic;
    public class OrderNormalizationProcessor
    {
        public List<FraudRadar.Order> Normalize(
            List<FraudRadar.Order> orders, 
            Action<FraudRadar.Order> normalizationHandler)
        {
            foreach (FraudRadar.Order order in orders)
            {
                normalizationHandler(order);
            }

            return orders;
        }
    }
}