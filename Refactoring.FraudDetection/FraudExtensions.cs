﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    public static class FraudExtensions
    {
        public static List<FraudRadar.FraudResult> ToFraudResultList(this List<FraudRadar.Order> orders)
        {
            var result = new List<FraudRadar.FraudResult>();

            foreach (FraudRadar.Order order in orders)
            {
                result.Add(new FraudRadar.FraudResult
                {
                    IsFraudulent = true,
                    OrderId = order.OrderId
                });
            }

            return result;
        }

        public static List<FraudRadar.Order> Normalize(this List<FraudRadar.Order> orders)
        {
            var orderNormalizationProcessor = new OrderNormalizationProcessor();
            var orderFieldNormalization = new OrderFieldNormalization();

            Action<FraudRadar.Order> normalizationHandlers = orderFieldNormalization.EmailFieldNormalized;
            normalizationHandlers += orderFieldNormalization.StateFieldNormalized;
            normalizationHandlers += orderFieldNormalization.StreetFieldNormalized;

            // Daniel: If needed is possible to add new handlers without modified existing ones

            orders = orderNormalizationProcessor.Normalize(orders, normalizationHandlers);

            return orders;
        }

        public static List<FraudRadar.Order> ToConsolidateList(this List<List<FraudRadar.Order>> listOfLists)
        {
            List<FraudRadar.Order> fraudList = new List<FraudRadar.Order>();

            foreach (List<FraudRadar.Order> list in listOfLists)
            {
                fraudList = fraudList.Union(list).ToList();
            }

            return fraudList;
        }
    }
}