﻿// -----------------------------------------------------------------------
// <copyright file="FraudRadar.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    public class EmailFraudDetectionSpec : ISpecification<FraudRadar.Order>
    {
        public bool IsSatisfied(FraudRadar.Order current, FraudRadar.Order next)
        {
            return current.DealId == next.DealId && 
                   current.Email == next.Email && 
                   current.CreditCard != next.CreditCard;
        }
    }
}