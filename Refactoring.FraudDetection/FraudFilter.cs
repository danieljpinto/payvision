﻿// -----------------------------------------------------------------------
// <copyright file="FraudRadar.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------


using System.Linq;

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    using System.Collections.Generic;
    public class FraudFilter : IFilter<FraudRadar.Order>
    {
        public IEnumerable<FraudRadar.Order> Filter(
            IEnumerable<FraudRadar.Order> items, 
            ISpecification<FraudRadar.Order> spec)
        {
            var list = items.ToList();

            for (int i = 0; i < list.Count; i++)
            {
                FraudRadar.Order current = list[i];

                for (int j = i + 1; j < list.Count; j++)
                {
                    FraudRadar.Order next = list[j];

                    if (spec.IsSatisfied(current, next))
                    {
                        yield return next;
                    }
                }
            }
        }
    }
}