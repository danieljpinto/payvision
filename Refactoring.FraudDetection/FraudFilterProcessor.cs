﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    public class FraudFilterProcessor
    {
        public List<FraudRadar.Order> GetFraudResults(IList<FraudRadar.Order> orders)
        {
            var fraudFilter = new FraudFilter();

            List<FraudRadar.Order> ordersWithEmailSpec =
                fraudFilter.Filter(orders, new EmailFraudDetectionSpec())
                    .ToList();

            List<FraudRadar.Order> ordersWithAddressSpec =
                fraudFilter.Filter(orders, new AddressFraudDetectionSpec())
                    .ToList();

            // Daniel: Of course both of validations could be one, but I want to demonstrate
            // how is possible to create another specification and added to the filter execution,
            // like Credit Card validation or something else including from another assembly.


            var listsToConsolidate = new List<List<FraudRadar.Order>>
            {
                ordersWithEmailSpec,
                ordersWithAddressSpec
            };

            return listsToConsolidate.ToConsolidateList();
        }


            
    }
}