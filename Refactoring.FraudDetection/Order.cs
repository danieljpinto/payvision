﻿// -----------------------------------------------------------------------
// <copyright file="FraudRadar.cs" company="Payvision">
//     Payvision Copyright © 2017
// </copyright>
// -----------------------------------------------------------------------

namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    public partial class FraudRadar
    {
        public class Order
        {
            public int OrderId { get; set; }

            public int DealId { get; set; }

            public string Email { get; set; }

            public string Street { get; set; }

            public string City { get; set; }

            public string State { get; set; }

            public string ZipCode { get; set; }

            public string CreditCard { get; set; }

            public override string ToString()
            {
                return $"OrderId: {OrderId}, DealId: {DealId}, Email: {Email}, Street: {Street}, City: {City}, State: {State}, ZipCode: {ZipCode}, CreditCard: {CreditCard}";
            }
        }
    }
}