﻿namespace Payvision.CodeChallenge.Refactoring.FraudDetection
{
    public class AddressFraudDetectionSpec : ISpecification<FraudRadar.Order>
    {
        public bool IsSatisfied(FraudRadar.Order current, FraudRadar.Order next)
        {
            return current.DealId == next.DealId
                   && current.State == next.State
                   && current.ZipCode == next.ZipCode
                   && current.Street == next.Street
                   && current.City == next.City
                   && current.CreditCard != next.CreditCard;
        }
    }
}